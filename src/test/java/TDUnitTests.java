import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Point;
import tdgame.FileParser;
import tdgame.enemies.Enemy;
import tdgame.enemies.Minion;
import tdgame.turrets.Archer;
import tdgame.turrets.Blaze;
import tdgame.turrets.Player;
import tdgame.turrets.Water;

import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class TDUnitTests {

    public int[][] getLocation() throws FileNotFoundException {
        File file = new File("levels/level1_path.txt");
        Scanner sc = new Scanner(file);
        int[][] loc=new int[sc.nextInt()][2];
        for(int i=0; i<loc.length; i++){
            loc[i][0]=sc.nextInt();
            loc[i][1]=sc.nextInt();
        }
        return loc;
    }

    @Test
    public void testEnemyGettingDamage() throws FileNotFoundException {
        Minion minion = new Minion(getLocation(),50,1,1,0,0);
        minion.takeDamage(30);
        assertEquals(minion.getHealth(),20,1);
    }


    @Test
    public void testBuyingTurretWithMoney()  {
        Player player = new Player(50,100);
        // Arbitrary point
        Point p = new Point(5,5);
        Archer archer = new Archer(p);

        player.buyTurret(archer);
        assertEquals(true,player.getTurrets().contains(archer));

    }

    @Test
    public void testBuyingTurretWithoutMoney(){
        Player player = new Player(50,0);
        Point p = new Point(5,5);
        Water water = new Water(p);
        boolean checkIfContains = player.getTurrets().contains(water);
        assertEquals(true,!checkIfContains);
    }

    // @Test
    // public void testUpgradingTurretWithMoney(){
    //     Player player = new Player(50,1000);
    //     Point p = new Point(5,5);
    //     Archer archer = new Archer(p);

    //     player.upgradeTurret(archer,0);
    //     assertEquals(1,archer.getLevel());

    //     player.upgradeTurret(archer,1);
    //     assertEquals(2,archer.getLevel());

    //     player.upgradeTurret(archer,2);
    //     assertEquals(3,archer.getLevel());
    // }




}
