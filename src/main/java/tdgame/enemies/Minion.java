package tdgame.enemies;

import tdgame.enemies.Enemy.type;


public class Minion extends Enemy{
    private static String name = "Ground";
    private static double health;
    private static double speed;
    private static double armor;
    private static int reward;    
    public static final int heigth=36;
    public static final int width=24;
    public int elemental;
    
    public Minion(int[][] Locations, double health, double speed, double armor, int reward, int damage){
	super(Locations, health, speed, armor, reward, damage, name, type.MINION);
        elemental=0;
    }
}